#!/bin/bash

# Funktion zur Ausgabe der Systeminformationen
print_system_info() {
    # Hostname
    printf "Hostname:\t%s\n" "$(hostname)"

    # Aktuelle IP-Adresse
    printf "Aktuelle IP-Adresse:\t%s\n" "$(hostname -I)"

    # Betriebssystemversion
    printf "Betriebssystemversion:\t%s\n" "$(lsb_release -d | cut -f2)"

    # CPU-Modell
    printf "CPU-Modell:\t%s\n" "$(lscpu | grep 'Model name' | awk '{$1="";print}')"

    # Anzahl der CPU-Cores
    printf "Anzahl der CPU-Cores:\t%s\n" "$(nproc)"

    # Gesamter und genutzter Arbeitsspeicher
    printf "Gesamter Arbeitsspeicher:\t%s\n" "$(free -h | awk 'NR==2 {print $2}')"
    printf "Genutzter Arbeitsspeicher:\t%s\n" "$(free -h | awk 'NR==2 {print $3}')"

    # Größe des verfügbaren Speichers
    printf "Verfügbarer Speicher:\t%s\n" "$(free -h | awk 'NR==2 {print $7}')"

    # Größe des freien Speichers
    printf "Freier Speicher:\t%s\n" "$(free -h | awk 'NR==2 {print $4}')"

    # Gesamtgröße des Dateisystems
    printf "Gesamtgröße des Dateisystems:\t%s\n" "$(df -h / | awk 'NR==2 {print $2}')"

    # Größe des belegten Speichers auf dem Dateisystem
    printf "Belegter Speicher auf dem Dateisystem:\t%s\n" "$(df -h / | awk 'NR==2 {print $3}')"

    # Größe des freien Speichers auf dem Dateisystem
    printf "Freier Speicher auf dem Dateisystem:\t%s\n" "$(df -h / | awk 'NR==2 {print $4}')"

    # Aktuelle Systemlaufzeit
    printf "Aktuelle Systemlaufzeit:\t%s\n" "$(uptime | awk '{print $3,$4}' | sed 's/,//')"

    # Aktuelle Systemzeit
    printf "Aktuelle Systemzeit:\t%s\n" "$(date '+%Y-%m-%d %H:%M:%S')"

    # Trenner für die nächste Ausgabe
    printf "\n-----------------------------\n\n"
}

# Prüfen der Optionen
if [[ $# -eq 0 ]]; then
    # Keine Option angegeben, nur Terminal-Ausgabe
    print_system_info
elif [[ $# -eq 1 && $1 == "-f" ]]; then
    # Option -f angegeben, zusätzlich Dateiausgabe
    log_file="$(date '+%Y-%m')-sys-$(hostname).log"
    print_system_info | tee -a "$log_file"
else
    # Ungültige Option
    echo "Ungültige Option angegeben!"
    exit 1
fi
